import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter)

const routes =[
    {
        path:'/',
        name: 'StartPage',
        component :()=>import(/* webpackChunkName : "StartPage" */ '../components/StartPage/StartPage') 
    },
    {
        path:'/GamePage',
        name: 'GamePage',
        component :()=>import(/* webpackChunkName : "GamePage" */ '../components/GamePage/GamePage.vue') 
    },
    {
        path:'/ResultPage',
        name: 'ResultPage',
        component :()=>import(/* webpackChunkName : "ResultPage" */ '../components/ResultPage/ResultPage.vue') 
    },
    {
        path:'/QuestionsPage',
        name: 'QuestionsPage',
        component :()=>import(/* webpackChunkName : "QuestionsPage" */ '../components/GamePage/QuestionsPage.vue') 
    }
]

const router = new VueRouter({
    mode : 'history',
    base : process.env.BASE_URL,
    routes 

})

export default router;