import Vue from 'vue'
import Vuex from 'vuex'
//import { StartPageApi } from '../components/StartPage/StartPageApi'

Vue.use(Vuex)

export default new Vuex.Store({
    state :{
        numberOfQuestions:[],
        answers: [],
        catagoryId:[],
        catagories:[],
        selectedCatagory:'',
        difficulties:[],
        fetchingError:'',
        fetching :false,
        result:''
        
    },
    mutations: {
        SetAnswers: (state, payload) => {
            state.answers=payload           
        },
        setNumberOfQuestions:(state,payload)=>{
            state.numberOfQuestions=payload
        },
        setAnswers (state) { 
            state.answers = []
        },
        setCatagories:(state,payload)=>{
            state.catagories=payload
        }, 
        setDifficulties:(state,payload)=>{
            state.difficulties=payload
        },
        setFetchingError:(state , payload)=>{
            state.fetchingError=payload
        },
        setFetching:(state,payload)=>{
            state.fetching=payload
        },
        SetResult: (state, payload) => {
            state.result=payload           
        },
    },  
    //this how we sent data to any compnent
    getters: {
        catagoryId: (state) => {
            var catagoryId = state.answers
            return catagoryId;
        }
    },

   actions:{
    SetAnswers: (context, payload) => {
    
            context.commit('SetAnswers', payload);     
    },

    async allTrivaApi(){

        try {
        const myrespone = await fetch("https://opentdb.com/api_category.php");
          this.catagories= await myrespone.json()
          .then(response=> response.json());
           commit('setCatagories',catagory.trivia_categories)
        } catch (error) {
          this.error = error.message
        }
      }
    //    async fetchCatagories({commit}){
    //     try {
    //         const catagory = await fetch("https://opentdb.com/api_category.php")
    //         .then(response=> response.json());
    //          commit('setCatagories',catagory.trivia_categories)
    //      } catch (e) {
    //         commit('setFetchingError',e.message)
    //      }
    //    },
      
    //    async fetchselectedCatagory({commit},){
    //     try {
    //         const catagory = await fetch(`https://opentdb.com/api.php?amount=${a}&category=${d}&difficulty=${b}&type=${c}`)
    //         .then(response=> response.json());
    //          commit('setCatagories',catagory.trivia_categories)
    //      } catch (e) {
    //         commit('setFetchingError',e.message)
    //      }
    //    } ,
    
        // resetData ({commit}) {
        //     commit('sendData')
        // }      
   } 


})

   

























